# Trevland

Мобильное меню - оригинально) `+` однозначно)

Хедер должен быть отдельно от контента страницы, картинка на всю ширину и текст - это уже контент страницы! Хедер можно было через `position: absolute` сделать

Как по мне, background-у не хватает `background-size: cover;`

Пора наверное подключить google шрифты) попробовать, разобраться )


# html
Этот блок не принадлежит хедеру, он должен быть в main. Сам хедер можно было `position: absolute;`  сделать
```html
<div class="main-header__content">
  <h1 class="main-header__title">Railtrips To Here, <br> There And Everywhere!</h1>
  <p class="main-header__descr">We all wish to start our year the best way possible and also according to a common
    belief if you have a great start to your.</p>
  <button class="main-btn">Explore more</button>
</div>
```

А так все хорошо) 

# css

Отлично)


# js

Префиксы  `js-` для javascript-а.

В html навешиваем класс `js-burger`, в css не используем его, этот класс живет только для js-a, и потом
```javascript 
const burger = document.getElementsByClassName('js-burger')[0];
```
для тогла стилей все остаётся как есть)


Еще не хватает проверки по какому элементу клик, ибо закрывается меню по клику области меню.
```javascript
body.addEventListener('click', () => {
  if (burger.classList.contains('is-active')) {
    navbar.classList.toggle('change');
    burger.classList.toggle('is-active');
    body.classList.toggle('overflow-hidden');
  }
})
```
```javascript
body.addEventListener('click', e => {
  if (!navbar.contains(e.target) && burger.classList.contains('is-active')) {
    ...
```
А так отлично)
