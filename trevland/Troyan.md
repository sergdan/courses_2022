# Trevland

Над мобильным меню еще бы поработать, добавить отступов между пунктами)
Ховеры: если используешь border, то его можно сразу задать прозрачным, а потом изменять цвет, чтобы избежать сдвигов)

Три варианта изображения, в реалиях такое редко возможно(


`from-blog__info` - поломал десктоп верстку (

# html

Бэм все равно хромает(

Элементов без блоков не может быть
```
booking__btn - блока booking нету
popular-locaion__section - можно оставить popular-locaion и тогда это будет блок для всех вложенных элементов
from-blog__section - аналогично 
services__block - тоже должен быть просто services.
```
По сути там где ты добавили `*__section` и `*__block` - это все блоки, эти постфиксы можно убрать, и тогда будет красота)

# css
Не фиксируем высоту body 
`body { max-height: 3271px; }`

Нулевым значения не нужны единицы измерения 
```css
.popular-locaion__cards {
  margin-top: 0px;
}
```
Не стилизуем теги  `.header__burger span`

`.from-blog__info` - тут что-то пошло не так( Можно реализовать по разному, сделать две колонки и не использовать `grid-template-areas`,  можно с использованием  `grid-template-areas`, но тогда нужно более детально название можно использовать абстрактные, не обязательно как класс
```css
.from-blog__info { 
  grid-template-areas:
    "big-info small-info-1"
    "big-info small-info-2"
    "big-info small-info-3";
}
.from-blog__big-info { grid-area: "big-info"; }

.from-blog__small-info--1 { grid-area: "small-info-1"; }

.from-blog__small-info--2 { grid-area: "small-info-2"; }

.from-blog__small-info--3 { grid-area: "small-info-2"; }
```


# js
В js рекомендовано использовать [ верблюжья нотация(сamelCase)](https://learn.javascript.ru/variables#variable-naming), но не критично) там же о let и const

Еще раз читаем почему [getElementsByClassName](https://www.dropbox.com/scl/fi/o9lgbks1936ngmvnhlh2i/JS.paper?dl=0&rlkey=kdjjd98rqcuvatd38hawwxksw#:uid=672203874793407432828702&h2=%D0%9A%D0%B0%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D1%82%D1%8C-%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82). И забываем о querySelector

Там же про префиксы  `js-` для javascript-а.

В html навешиваем класс `js-burger`, в css не используем его, этот класс живет только для js-a, и потом
```javascript 
const menuButton = document.getElementsByClassName('js-burger')[0];
```
для тогла стилей все остаётся как есть)

`-` за закрытие меню по клику на меню, а точнее по блочному элементу <li> списка, это же не сама ссылка, но `+` за старание)
