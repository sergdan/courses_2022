# Trevland

Хорошо) видно, что постаралась)

# html

БЭМ - заметно лучше)

Не забываем, модификатор не используем без блока или элемент
```html
<div class="grid__item--1">
<li class="footer__row--email">
```

Кнопки `type="submit"` желательно использовать только в формах) а так `type="button"`)

# css

Что-то ты запуталась в классах 

`.accent-background` - `.accent-block` - `.accent-block `

Сначала все было хорошо, 
```css
.accent-block {
   background: url(../img/main_picture.png);
   background-size: cover;
   background-position: 10% 100%;
}
.accent-background {
   margin-bottom: 50px;
}
```
На медиа 1200, у `.accent-block` меняется картинка, но тут же перезатераешь `background-image` => сокращенной записью `background` ([почитать тут](https://developer.mozilla.org/ru/docs/Web/CSS/background) , лучше запомнить данное). И тут входит в игру `.accent-background` уже с main_picture.png, у которого нет свойства `background-size: cover;`, и все таки, ширину не нужно было ограничивать, `width: 1600px;` => `width: 100%`, чтобы картинка была на всю ширину в десктопе)
```css
.accent-block {
  background-image: url(/img/Rectangle\ 32.png);
  background: var(--color-box);
  border: 1px solid var(--theme-main-text);
  box-sizing: border-box;
  box-shadow: 0px 4px 4px var(--color-box);
  width: 1600px;
  height: 900px;
}
.accent-background {
  background-image: url(../img/main_picture.png);
  width: 1600px;
  height: 900px;
  margin: 0 auto 140px auto;
}
```

Хедер тоже нужно было в контейнер обернуть)

Свойства уже имеют дефолтные значение, ну нужно их дублировать. Так нужно сделать лишь в случаи, когда тебя родителю задано иное значение, и тебе нужное его переопределить.
```css
  font-style: normal;
  font-weight: normal;
```

# js

Хорошо)

tip: `console.log('${scrollY}px');` => нужно использовать обратные кавычки, подсмотреть [тут](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Template_literals)

Еще бы на `body` `overflow: hidden` навесить при отрытом бургере, запретить скролл)