# Trevland

`.background-top__img` не хватает `object-fit: cover;`

Адаптив в целом неплохо, но не до конца)

"From Blog & News"  - в мобилке не очень)


# html


Для начала это не кнопка, это должно быть ссылкой. Блока `card` - не существуют, а значит не может быть его элементов.
```html
<div class="block-button-cards__cards-section">
  <button class="cards-section__card">
    <img class="card__image" src="./img/Istanbul.png" alt="Istanbul">
    <h3 class="card__tittle">Istanbul, Turkey</h3>
    <p class="card__text">Istanbul is a major city in Turkey that straddles Europe. </p>
  </button>
</div>
```
Не использовать модификаторы без самого блока или элемента. 
```html
<div class="content__section--top-section">
```
Это должно быть формой
```html
<nav class="footer-nav">
  <h2 class="footer-nav__tittle">SUBSCRIBE NOW</h2>
  <input class="footer-nav__input" type="text" placeholder="Email">
  <button class="footer-nav__button button">Send</button>
</nav>
```

# css

Уменьшить в размера для мобилки: 
`transform: scale(0.7);` - что-то новенькое... так никогда не делаем больше!!! 

Fixed - возможные варианты это header или кнопка Go to top.
```css
.card__image {
    width: 100%;
    height: 80%;
    position: fixed;
    top: 0;
    left: 0;
}
```
Не забываем о сокращенной записи
```css
.block-blog__content {
  grid-template-rows: 75px 75px 75px; /*=> grid-template-rows: repeat(3, 75px); */
  grid-template-columns: 250px 250px; /*=> grid-template-columns: repeat(2, 250px); */
  grid-gap: 2.5px 2.5px; /*=> grid-gap: 2.5px; */
}
```
# js

Есть) но нету самого меню(