# Trevland

C мобильной версткой беда( 

Бергер отсутствует( main.js - не существует или ты забыла его добавить в архив?!

Footer-меню просто списки, должны быть ссылки(

# html

По БЭМ - хорошо)

`<li class="legal__item__first">Legal</li>` - тут должен быть модификатор. `<li class="legal__item legal__item--first">Legal</li>`

# css
Верстку делаем mobile-first. `@media (max-width: 800px)`  -> `@media (min-width: 800px)`

@media группируем, дабы не дублировать одно и то же

Css-переменные не везде

За основу лучше взять предложенный список контейнеров?
```css
  --mq-size-sm: 576px;
  --mq-size-md: 768px;
  --mq-size-lg: 992px;
  --mq-size-xl: 1200px;
```

Изменяем одно свойство
```css
margin: 0 0 20px 0; => margin-bottom: 20px;
```

`font-family: 'Roboto', sans-serif;` - не дублируем, свойство `font-family` наследуется от родителя, и ты его уже задала на body.

`font-weight: 400;` - аналогично, наследуется. + `400` это дефолтное значение, его вообще можно не писать.
# js

-