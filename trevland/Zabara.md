# Trevland

Рекомендую подключение шрифтов перенести в html, так они будут немного быстрее загружаться, ибо если через css их подключать, то нужно ждать когда загрузятся стили, и только потом браузер начнет загружать шрифты.

# html
Хорошо) 

`news__item grid-2` - grid-2 можно использовать как модификатор, так как `grid-2` все же относиться к контексту блока `news`

# css

Не забываем, нулевым значение не нужны единицы измерения, и не забываем точечном задании стилей
```css
.footer__img {
  margin: 0px 0px 35px; => margin: 0 0 35px; => margin-bottom: 35px;
}
```
  
Не злоупотребляй `z-index: 1000;` , начинай с цифр поменьше)


Класс `.checked` должен влиять только на `burger`, + потом в js минус один тоглер класса `burBox.classList.toggle('checked')`

Поэтому стилизовать нужно такие селекторы, немного усложняем, но избежим проблем, когда где-то еще появится класс `checked`
```css
.burger.checked .burger__item {}
.burger.checked .burger__item:before {}
.burger.checked .burger__item:after {}
```

# js

Все хорошо, кроме... где префиксы `js-` для классов, которые используем для получения элементов!?

```javascript
const bur = Array.from(document.getElementsByClassName("burger"))
```
В html прописываем класс `js-burger`, в css данный класс не используем, заведома зная что он для js.
```javascript
const bur = Array.from(document.getElementsByClassName("js-burger"))
```
`menuBox.classList.toggle('is-active')` с классами, которые добавляем для стилизации, ничего не нужно делать, тут все верно)
