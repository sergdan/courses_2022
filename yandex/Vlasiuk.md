# Yandex


Попробуй подключить `<picture>` `<source>` [srcset](https://developer.mozilla.org/ru/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images#%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%80%D1%8B)
для главной картинки)

Нету смысла в доп классах `mattress linens pillow`, изменять order - а есть ли смысл?!)
```html
<li class="inside__item mattress">Ортопедический матрас</li>
<li class="inside__item linens">Постельное бельё</li>
<li class="inside__item pillow">Подушка с эффектом памяти</li>
<li class="inside__item plaid">Тёплый плед</li>
<li class="inside__item charger">Розетка для зарядки</li>
<li class="inside__item wi-fi">Wi-Fi</li>
```
       
А для `margin-left: 0;` можно `.inside__item:nth-child(3n)`

# html

`<p class="more__descr-colored">` - модификатор без элемента не используем

Блок "Как это работает?" лучше сделать как нумерованный список


# css

`font-weight: normal;`  23 раза повторяется) дефолтное значение)
`font-size: var(--main-text-size);` - красота)

Лучше сделать элемент для позиционирование кнопки
```css
.works .main-btn { => .works__btn
  width: 204px;
  height: 48px;
  margin-top: 16px;
}
```
Не все цвета в css-переменных(

# js

`maps.InfoWindow` - `+`
js красота)

Попробуй слайдер подключить, [дока для сборщика](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/_.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta#:uid=064708122015221663202080&h2=Slick-carousel) или через [cdn](https://kenwheeler.github.io/slick#go-get-it)

