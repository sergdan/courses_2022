# Yandex
conteiner - опечатка, должно быть container :-)

http://joxi.ru/v29wx0Dt4ZyOOr - нехватает линии)

`.testimonials__slider` -  картиночка) все таки нужно было сверстать)

И если будет время, нужно подключить слайдер

[Дока](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/_.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta) как подключить слайдер

И 
```css
.main {
   background-image: url('../img/main-picture_little.png');
}
```
Попробуй разобраться с [srcset](https://developer.mozilla.org/ru/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images#%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%80%D1%8B)
```html
<picture class="place__block">
  <source srcset="./img/main-picture_little.png" type="image/png" media="(max-width: 576px)">
  <source srcset="./img/main-picture_middle.png" type="image/png" media="(max-width: 768px)">
  <source srcset="./img/main-main_picture.png" type="image/png" media="(max-width: 1200px)">
  <img src="img/location_1200.png" alt="map">
</picture>
```

# html

Красота) несколько опечаток: `prise__box`, `price-button`

# css

Отлично)



# js

Стилизацию класс .scroll можно было просто хедеру навесить, по сути, нету смысла в данных действия) но, зато, практика в js)
```javascript
header.classList.add('scroll');
```

Не изменяем стили через js, навешиваем классы!)
```javascript
if (panel.style.display === "block") {
  panel.style.display = "none";
}
```

Отлично)
нехватает блокирования скролла при открытом бургер меню. 
```css
.blocked {
  overflow: hidden;
}
```
```javascript
burgers.addEventListener("click", function (e) {
    document.body.classList.toggle('blocked')
    burgers.classList.toggle('is-active');
    navigations.classList.toggle('is-active');
  });
```
