# Yandex

Отлично)

Попробуй подключить `<picture>` `<source>` [srcset](https://developer.mozilla.org/ru/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images#%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%80%D1%8B)
для главной картинки)
# html
Супер)

# css

Красота) 

```scss
@import '../../node_modules/slick-carousel/slick/slick.scss';

Можно так подключить
@import 'slick-carousel/slick/slick.scss'
```

в файликах `.sass` можно сократить синтаксис, не писать фигурные скобки и точку с запятой

```scss
.footer 
  width: 100%
  height: 108px
  background: radial-gradient(100.78% 106.26% at 33.46% 18.73%, var(--theme-primary-gradient1) 0%, var(--theme-primary-gradient2) 100%)
  
  &__text 
    text-align: center
    padding-top: 44px
```


# js

burger.js
```javascript
const burgersMenu = Array.from(document.getElementsByClassName('js-nav'));
const burgers = Array.from(document.getElementsByClassName('js-burger'));

burgers.forEach(burger => {
  burgersMenu.forEach(burgerMenu => {
  });
});
```
Лучше взять первый элемент, дабы потом не было циклов в циклах
```javascript
const [burgerMenu] = Array.from(document.getElementsByClassName('js-nav'));
const [burger] = Array.from(document.getElementsByClassName('js-burger'));
```
