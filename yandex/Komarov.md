# Yandex

Не нужно дублировать весь блок подключения шрифта, их же можно указать в одном запросе)

```html
 <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:wght@300;400;600;700&display=swap&subset=cyrillic"
    rel="preload" as="style">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:wght@300;400;600;700&display=swap&subset=cyrillic"
    rel="stylesheet" media="print" onload="this.media='all'">
  <noscript>
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat:wght@300;400;600;700&display=swap&subset=cyrillic"
      rel="stylesheet">
  </noscript>
```
```html
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Didact+Gothic&family=Montserrat:wght@300;400;600;700&display=swap" rel="stylesheet"
```

http://joxi.ru/EA4wRJ1tXOwvJ2 - вот поэтому нужно всегда делать врапер для картинок))

# html
Хорошо)

Иногда опечатки) `section-container-third

И тут что-то пошло не так в span вложены h3 & p
```html
<div class="section-container__item section-container__item--fifth">
      <span class="number"><p class="text">5</p></span>
      <span class="text-wrapper">
        <h3 class="subtitle">Отдохнув, отдаёте ключ</h3>
        <p class="text">И с новыми силами продолжаете заниматься делами!</p>
      </span>
  </div>
```

# css

в html у тебя есть блок section и модификаторы, а по факту у тебя все модификаторы это самостоятельные блоки http://joxi.ru/bmoNqDdUO9xd9r
```sass
@import section-general
@import section-description
@import section-work
@import section-location
@import section-price
@import section-feedback
@import section-questions
```
Нужно дробить мельче, стилизиция секций отвечает за секцию в целом, и немного за первую вложенность(элементы), а дальше уже идут новые блоки, стили которых мало зависят от секции.

У тебя сейчас размер шрифта текста зависит от секции http://joxi.ru/Q2KXRW4HOw4WBA , слишком глубокая вложенность

Аккордеон будет работать только в сеции  `.section--questions .accordion-item__title`, а если нужно будет применить аккордеон в другой секции, будет копипаст...

В html все красиво по БЭМ, нужно теперь этот БЭМ в стилях применить )

# js

burger.js
```javascript
const burger = document.getElementsByClassName('header-burger') [0]; // -> js-header-burger
const menu = document.getElementsByClassName('header-menu') [0]; // -> js-header-menu
const btn = document.getElementById('btn'); // -> js-burger-btn  и почему тут getElementById
const wrap = document.getElementsByClassName('header-wrapper__bg')[0]; // -> js-header-wrapper-bg

// тут все так и остаётся
burger.addEventListener("click", function (e){
   this.classList.toggle('header-burger--is-active');
   menu.classList.toggle('header-menu--is-active');
   btn.classList.toggle('button--is-visible');
   wrap.classList.toggle('header-wrapper__bg--is-change')
});
```

Переменная marker объявляется в каждой итерации forEach, там же нужно и добавлять addListener
```javascript
  features.forEach(el => {
    const marker = new google.maps.Marker({
      position: el.position,
      icon: icons[el.type].icon,
      map: map,
    });
  });


  marker.addListener("click", () => {
    infowindow.open({
      anchor: marker,
      map,
      shouldFocus: false,
    });
  });
```