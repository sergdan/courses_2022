# Yandex

Адаптив для планшета пострадал от второго слайда( http://joxi.ru/L21wK7ltwggR8r
# html
БЭМ :-(

Блоков "main-image" и "secondary-image" нету, а значит, `main-image__picture` и `secondary-image__picture` элементов не может быть.
```html
<div class="header-logo">
  <div class="header-logo__main-image">
    <img src="./img/header/yandexlogo.svg" alt="yandex logo" class="main-image__picture">
  </div>
  <div class="header-logo__secondary-image">
    <img src="./img/header/skilltrain.svg" alt="skilltrain" class="secondary-image__picture">
  </div>
</div>
```
И еще стили для данных элементов одинаковые
```css
.main-image__picture{
    display: block;
    width: 100%;
    height: auto;
}
.secondary-image__picture{
    display: block;
    width: 100%;
    height: auto;
}
```
тогда можно было `header-logo__image` класс для картинок и все супер)

Где-то с БЭМ запутался. Еще раз [почитать](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/-%D0%9A%D0%B0%D0%BA-%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D1%82%D1%8C-%D0%BA%D0%BB%D0%B0%D1%81%D1%81-%D0%B2-HTML-_.paper?dl=0)

Элементов без блока не может быть, не забывай.

# css


`font-style: normal;` -повторяется 31 раз)) дефолтное значение) нужно подчищать копипаст с фигмы)

все цвета в css-переменные!)

В целом хорошо, нужно подтянуть БЭМ, и стили станут круче)



# js
Хорошо)

Аккордеончики?

Попробуй слайдер подключить, [дока](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/_.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta#:uid=064708122015221663202080&h2=Slick-carousel) для сборщика или через [cdn](https://kenwheeler.github.io/slick#go-get-it)
