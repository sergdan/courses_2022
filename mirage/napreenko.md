Хедеру не хватает фона)

# html

Семантика - хорошо)

В css половины блоков нету) так может это лишние комбинации блок-элементов
БЭМ - это не только для html, это как раз больше к css относиться)

Еще раз БЭМ [тут](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/_-HTML-_.paper?dl=0)

Не нужно на каждую вложенность создавать блок(причем он только в html), `iner__part` и `part` нету в css, так может и данная вложенность не нужна
```html
<div class="footer__iner iner">
  <div class="iner__part part">
    <div class="part__logo">
```

`footer__ddescription` - это футерное меню)

# css

Дублировать `font-family: Montserrat;` каждый раз не нужно, можно на верхнем уровне задать, и если он значение свойства не меняется, то и переопределять не нужно)

Зачем модификатор блока, если нету стилей для блока. Второе, модификатор блока может влиять на стили элементов.
```css
.card-service--themes {
    background-color: var(--theme-primary-main);
}

.card-service__title--theme {
    color: var(--theme-text-1)
}

.card-service__text--theme {
    color: var(--theme-text-1)
}
```
ПО второму, селекторы элементов будут выглядеть так
```css
.card-service--themes .card-service__title {
    color: var(--theme-text-1)
}
```
# Итого :-)

Над мобильным вариантом можно было еще поработать)
Стили в целом - хорошо)
Нужно осознать смысл БЭМ-а, почитать [официальную документацию](https://ru.bem.info/methodology/quick-start/), не забывая о [наших правилах именования](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/_-HTML-_.paper?dl=0&rlkey=maxb5bkctm6zooh9dj3i230ri#:uid=762484354494646660598118&h2=%D0%9E%D1%81%D0%BE%D0%B1%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8)