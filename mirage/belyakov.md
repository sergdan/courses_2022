
# html

Семантика - хорошо)

`content-block__description` - элементы не существуют без блоков.

Еще раз БЭМ [тут](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/_-HTML-_.paper?dl=0)

Почему это все элементы блока `button`, которого даже не существует
```html
<img class="button__button-image" src="img\button-design.png" alt="design">
<h2 class="button__button-tittle">Design</h2>
<p class="button__button-text">a plan or drawing produced to show the look and function</p>
<a class="button__button-link" href="">Learn more <img src="img\triangle.png" alt="triangle"></a>
```

# css
Напоминание о том как делать адаптив [тут](https://www.dropbox.com/scl/fi/de1y8rh04denhypoy8k7d/_-Mobile-first.paper?dl=0&rlkey=af5etct8729sesdxtlk3radbd)

Дефолтных стилей нету( на усмотрение браузера после reset-а браузерных стилей(  media-query 350px можно убрать, стили оставить они будут как первичные, и потом с увеличение экрана их подправлять

Не нужно дублировать все стили в каждом media-query, css - каскад, он накладывает стили поверх предыдущих) Ты их уже задал)

Комбинирования селекторов - данный подход не верный, нужно понять БЭМ, и тогда все станет на свои места)
```css
.content-block__text.content-block__text--testimonial-adress {}
```

`position: absolute;` - сводим к минимуму.

# Итого :-)

Подтягиваем БЭМ, изучаем [каскадность css](https://developer.mozilla.org/ru/docs/Web/CSS/Cascade)

Освоишь это - будет успех, все станет на свои места)