Копирайт с футера потерялся :-(

В дизайне некоторые элементы немного отличаются от идентичных, так вот, дизайнеры таким образом показывают нам какой должен быть ховер-эффект. Карточка Services, иконка твиттера)

Если эти блоки похожи по стилям, то можно было назвать универсально `section`, а для margin-ов добавить модификаторы, `section--agency`
```css
.agency__section,
.servies__section,
.design-things__section,
.our-teamwork__section,
.testimonial__section,
.brends__section {
  display: flex;
  justify-content: space-between;
  flex-wrap: nowrap;
  align-items: center;
}
```
Или отступы можно задать блоку, и тогда бы не было элементов без блоков
```html
<section class="section agency">
```

На мобилке спрятать футерное меню - бывает такое, но только если такие же ссылки есть в бургер меню, чаще нужно просто адаптировать) 
# html

Карточка сервиса должна быть ссылкой)

БЭМ: 
- `header__item__active` - модификатор. Модификаторы пишем через `--`
- `agency__section, servies__section, info__block` -  элемент не может существовать без блока




Семантика:
 - Карточки сервиса в `article` - не совсем семантично, тут больше `section` подходит
 - Design Things, Our Teamwork, Testimonial - тут можно было применить `article`
 - То что выглядит как кнопка - не всегда должно быть `button`, "Learn more" - это явно ссылки. Нужно ссылку стилизовать как кнопку.
 - Кнопкам лучше задавать тип `<button type="button">` или `<button type="submit">`(если это кнопка отправки формы)

# css
`.header:before ` - существует лишь для задания фона? его можно напрямую `.header`-у задать

Свойство `padding` не принимает отрицательные значения, а для того чтобы выровнять контент кнопки нужно было переопределить `padding-top: 0;`

Не стилизуем теги, стилизуем классы; Нулевым значения не нужны единицы измерения;
```css
.header__list li {
  margin: 0px 0px 0px 65px;
}
```

Если убрать `position: absolute;` то все прекрасно стает на свои места, зачем он здесь?
```css
.our-teamwork__image {
  position: absolute;
  display: block;
  width: 591.04px;
  height: 587px;
  object-fit: cover;
  margin-left: 578px;
}
```

О ховер-эффектах - забыла(
# Итого :-)

Семантика - нормально, БЭМ - нужно еще [почитать](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/-%D0%9A%D0%B0%D0%BA-%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D1%82%D1%8C-%D0%BA%D0%BB%D0%B0%D1%81%D1%81-%D0%B2-HTML-_.paper?dl=0), стили - хорошо)