В мобилке у тебя как-то все плотно получилось) http://joxi.ru/krDK6P4uG00KZA

По хорошему, блоки перестраивать, сначала картинка, потом контент к картинке http://joxi.ru/xAe3npECMYYRZm

dropdown переключения языков - список языков нужно было над самим полем реализовать, так бы не было проблем)

# html
Для картинок лучше иметь враппер, оборачиваю саму картинку в div-чик, потом с ней легче бороться)

`card__link` - тут опечатка(будем так считать), у тебя нету блока `card`, есть только `card-item`.

А нет, не опечатка - элементы без блоков не существуют `section__card`, `block__img`, `block__feedback`  и тд. Наверное все где ты писал `block-*` ты имел ввиду что это и есть блок. НО... синтаксис немного не такой, должно быть `feedback` - это и есть блок) и вот тогда `feedback__part` - элемент.
Почитай [нашу доку](https://www.dropbox.com/scl/fi/w7opxkcla4v2zci1qg9ln/_-HTML-_.paper?dl=0&rlkey=maxb5bkctm6zooh9dj3i230ri) 

Блога браузеры умные пошли, открываешь h2 закрываешь h1 `<h2 class="title">Design Things</h1>`

Лучше оставить все таки ссылкой) `button` `href=''`
```html
<button class='cl-button' href=''>
  <p>Learn more </p>
  <img src="assets/img/btn_icon.svg" alt="" class="cl-button__img">
</button>
```

# css
Для мобильного меню можно задать 
```css
@media (min-width: 576px)
.navigation.active {
    top: 100%;
}
```

Если все будет по БЭМ-у, то не нужно будет делать стили для сложных селекторов `.block__subscribe .subtitle`

Картинки со сторонних ресурсов используем по минимум
`background: url('https://icons.iconarchive.com/icons/wikipedia/flags/16/UA-Ukraine-Flag-icon.png') no-repeat left center;`

css-переменные есть, но все равно цвета hex-цвета во везде(

Нулевые значение не требуют единиц измерения `margin: 0px 0px 8px 10px;`
# js

C `.js-hovered-card` все получилось, а почему тут используется класс без префикса `.burger` ?)

Скажем так, попробовал, узнал как работает, больше не используй. Css - hover никто не отменял, и без лишнего js-a
```javascript
$('.js-hovered-card').hover(function(event) {
    $(this).toggleClass('active');
});
```

# Итого :-)

Запутано все, на первый взгляд - нормально) но тебя выбивает с колеи БЭМ. Подтяни БЭМ, тогда и css будет красивее)
