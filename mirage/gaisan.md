Хедеру не хватает вертикальных padding-ов, чтобы когда он становиться sticky, были небольшие отступы) и на эти 20px уменьшить margin, и задать бы ему фон)
```css
padding-top: 20px;
padding-bottom: 20px;
```

Чтобы можно было изменять цвет иконок ("наши сервисы, соц-сети) нужно их вставлять как svg, тогда помощью css свойств fill и stroke можно изменять цвет.


# html

БЭМ: 

`<a class="navigation__link navigation__link_active">` - ну нет( это же модификатор, или так `navigation__link--active`, или можно как дополнительный класс  `<a class="navigation__link is-active">`, таким образом обозначаем состояние данного элемента, а в css будет так
```css
.navigation__link.is-active {}
```
Ага, `about__rows_bold, about__cols_last` - это у тебя модификаторы, обсуждали же модификаторы пишем через `--`

В `img` атрибутам width и height не нужно задавать единицы измерения, браузер по умолчанию считает данные значения как px.
```html
<img class="partners__img partners__img_other" src="img/ferrari.png" alt="logo" width="170px"
                  height="100px">
```

Такие иконки лучше объединить при экспорте с фигмы.
```html
<img class="use-button__arrow" src="img/arrow.png" alt="arrow">
<img class="use-button__square" src="img/square.png" alt="sguare">
```

В твоём футере нужно было добавить контейнер, и можно `justify-content: space-between;`

Вообще нижняя секция с меню тоже должна быть футером

Блок `about` - это меню навигации по сайту


# css
`product__line` забыла за эту линию (

Нужно было при ховере на `group__block` изменять цвет шрифта вложенных  блоков
```css 
.group__block:hover .group__title, 
.group__block:hover .group__text,
.group__block:hover .group__article {
  color: var(--theme-button-text);
}
```

В стилях медиа-выражений ты опечаталась `--conteiner-width`.

Отрицательные margin-ы редко в жизни нужны, максимум для наложения одного блока на другой. Тут можно задать и положительный с другой стороны `margin-left: 30em;`
```css
.navigation__list {
   display: flex;
   margin-right: -30em;
}
```
А еще лучше задать сделать класс `header__navigation` который будет на том же `<nav class="navigation header__navigation">`:
```css 
.header__navigation {
  margin-left: auto;
}
```

# Итого :-)

В отдельности: БЕМ(кроме модификатор) - хорошо, html/семантика - нормально, 
стили - хорошо

В целом: Не старалась :-( 
