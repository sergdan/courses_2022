# Сергей Корниенко Instagram

## HTML / БЭМ

Особо вопросов нет, все достаточно корректно

Это должно быть ссылками, вместо спана

```html
<li class="public-list__item"><span class="public-list__count">79</span> posts </li>

```
Карточка поста, должна быть независимой от родителя. Она сама должна быть постом.
```html
 <div class="post-list">
  <div class="post-list__item">
</div>
```
Вот так
```html
<div class="post-list">
  <div class="post-item">
</div>
```
## СSS

Почему то почти все картинки в `background-image`. Тебе так легче? 
У меня просто нет такой любви к беграундам. А с появлением `object-fir: cover` тем более.

Плюс практически везде отсутствуют ховеры

Если в `font-face` несколько `src` - загружается последний. ЧТобы добиться проверки подходящих форматов, нужно оставить что-то одно)
```css
@font-face {
    font-family: 'Helvetica Neue';
    src: url('../fonts/helveticaneuecyr-medium-webfont.eot');
    src: url('../fonts/helveticaneuecyr-medium-webfont.eot?#iefix') format('embedded-opentype'),
    	 url('../fonts/helveticaneuecyr-medium-webfont.svg') format('svg'),
         url('../fonts/helveticaneuecyr-medium-webfont.woff') format('woff'),
         url('../fonts/helveticaneuecyr-medium-webfont.ttf') format('truetype');
    font-weight: 600;
    font-style: normal;
}
```

Чем писал? Откуда вендорные префиксы?? 

```css
.head {
    height: 53px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    
    -webkit-box-align: center;
    
        -ms-flex-align: center;
    
            align-items: center;
}
```

В общем чистенько, ровненько
