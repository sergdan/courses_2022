# Байда Евгения Instagram

## HTML

Небольшая опечатка, а то пойдет в след проект

```
content="...,use-scalable=0"
```
там `user-scalable=0` речь идет о возможности юзера скейлить экран)

Сразу бросился в глаза серый фон бекграунда ... Какая разная цветопередача, а код цвета один в один с фигмы

У меня не отображаются иконки...
http://joxi.ru/zANnZwlC8M6NDA 

Из-за того что локалка... https://evgeniya-star-35.github.io/first-HW/ 
Проверила у тебя - все ок

Семантически - супер!

По БЭМ-у - неплохо) Можно помельче делать блоки) 

Эти ссылки уже лишние в `about-me`.

```html
<ul class="about-me__list">
  <li class="about-me__item">
    <p class="about-me__paragrag">
      <a href="" class="about-me__details"
        ><span class="about-me__number">79</span
        ><span class="about-me__name"> posts</span></a
      >
    </p>
  </li>
```
Их можно отдельным блоком сделать
И обертка в `p` тут излишня. Она не несет в себе ничего) кроме увеличения DOM-дерева
```html
<ul class="user-navigation">
  <li class="user-navigation__item">
    <a href="" class="user-navigation__details"
      ><span class="user-navigation__number">79</span
      ><span class="user-navigation__name"> posts</span>
    </a>
  </li>
```

В Этом заголовке ты используешь только модификатор. 

```html
<h2 class="about-me__title--secondary">Name</h2>
```
Должно быть так . Модификатор не используется в отдельности

```html
<h2 class="about-me__title about-me__title--secondary">Name</h2>
```

Нижные сторисы http://joxi.ru/Rmz0O7lHjaRKkA я бы тоже отвязала от блока эбаут ми.

Делая такие большие блоки мы теряем главную прелесть бема. Переиспользование блоков. А так как мы не можем юзать элемент без блока, то с большей долей вероятности будут проблемы и копипаст , когда нам в другом месте пригодится, к примеру, сторис

И самая распространенная ошибка - карточка должна быть отдельным блоком. Карточки на 99% переиспользуемый элемент

http://joxi.ru/4Ak7wWxs0zX7JA

```html
<ul class="gallery__list">
  <li class="gallery__item">
    <a href="" class="gallery__link"
      ><picture class="gallery__picture">
        <source
          srcset="
            ./images/gallery/webp/img@1x.webp 1x,
            ./images/gallery/webp/img@2x.webp 2x
          "
          type="image/webp"
          media="(min-width: 295px) "
        />
        <source
          srcset="
            ./images/gallery/jpg/img70@1x.jpg 1x,
            ./images/gallery/jpg/img70@2x.jpg 2x
          "
          type="image/jpeg"
        />
        <img
          src="./images/gallery/jpg/img70@1x.jpg"
          alt="post"
        /> </picture
    ></a>
  </li>
```

Тем более у тебя ссылка и картинка вообще оказались детьми `gallery`, аж всей секции
Как вариант так. Карточка сама по сеьье. Список - сам по себе
```html
<ul class="gallery-list">
  <li class="gallery-list__item">
    <a href="" class="gallery-item"
      ><picture class="gallery-item__picture">
        <source
          srcset="
            ./images/gallery/webp/img@1x.webp 1x,
            ./images/gallery/webp/img@2x.webp 2x
          "
          type="image/webp"
          media="(min-width: 295px) "
        />
        <source
          srcset="
            ./images/gallery/jpg/img70@1x.jpg 1x,
            ./images/gallery/jpg/img70@2x.jpg 2x
          "
          type="image/jpeg"
        />
        <img
          src="./images/gallery/jpg/img70@1x.jpg"
          alt="post"
        /> </picture
    ></a>
  </li>
```

## CSS

```css
.about-me__card:hover{
  transform: scale(1.15);
  box-shadow: 0 10px 10px -7px #000,1px 3px 7px 6px var(--accent-color);
  transform: translateY(-10%);
}
```
В этом коде скейл работать не будет) Тут работает каскадность. Сначала он думает, что будет скейлиться, а потом видит что нужен сдвиг по оси Y и забывает про скейл.
Если хочешь вместе нужно:

```css
.about-me__card:hover{
  box-shadow: 0 10px 10px -7px #000,1px 3px 7px 6px var(--accent-color);
  transform: scale(1.15) translateY(-10%);
}
```

И еще всем анимация не хватает `transition: 0.5s` , к примеру) было бы намного  нежнее. ПОтому что ховеры грубоватые получились. Хоть я жуткий фанат градиентов  в любом виде))

В Целом: внешний вид - хорошо) семантика - ок) Бем - помельче) Стили - ок)
